/// #[derive(...)] statements define certain properties on the enum for you for
/// free (printing, equality testing, the ability to copy values). More on this
/// when we cover Enums in detail.

/// You can use any of the variants of the `Peg` enum by writing `Peg::B`, etc.
#[derive(Clone, Copy, Debug, Eq, PartialEq)]
pub enum Peg {
    A,
    B,
    C,
}

/// A move between two pegs: (source, destination).
pub type Move = (Peg, Peg);

/// Solves for the sequence of moves required to move all discs from `src` to
/// `dst`.
/// solve for 3
/// 3  | |
/// 2 |   |
/// 1|     |
///     A               B           C
/// C3, B2, C2, A3, C1, B3, C3
pub fn hanoi(num_discs: u32, src: Peg, aux: Peg, dst: Peg) -> Vec<Move> {
    let mut rv = vec![];
    // create vec containing disks (ascending?)
    // move two disks one peg
        // put smallest disc on dest
        // put next smallest on aux
        // put smallest on aux

    rv
}
