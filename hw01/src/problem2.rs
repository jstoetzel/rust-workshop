/// Represents a matrix in row-major order
pub type Matrix = Vec<Vec<f32>>;

// return product of vector elements
// vec_prod([1., 2.]) == 2.
fn vec_prod(vec: &Vec<f32>) -> f32 {
    let mut rv:f32 = 1.;
    for i in vec { rv *= i }
    rv
}

/// Computes the product of the inputs `mat1` and `mat2`.
pub fn mat_mult(mat1: &Matrix, mat2: &Matrix) -> Matrix {
    // the number of columns in the first matrix has to be
    // equal to the number of rows in the second matrix.
    let mat1_cols = mat1.len();
    let mat2_rows = mat2[0].len();
    assert_eq!(mat1_cols, mat2_rows);

    let mut rv = Matrix::new();
    for i in 0..mat1.len() {
        for j in 0..mat2.len() {
            let row_prod = vec_prod(&mat1[i]);
            let col_prod = vec_prod(&mat2[j]);
            rv[i][j] = row_prod + col_prod;
        }
    }
    rv
}
