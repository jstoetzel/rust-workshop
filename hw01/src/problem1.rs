
/// Computes the sum of all elements in the input i32 slice named `slice`
pub fn sum(slice: &[i32]) -> i32 {
    let mut rv = 0;
    for item in slice {
        rv += item
    }
    rv
}

/// Deduplicates items in the input vector `vs`. Produces a vector containing
/// the first instance of each distinct element of `vs`, preserving the
/// original order
pub fn dedup(vs: &Vec<i32>) -> Vec<i32> {
    let mut rv: Vec<i32> = Vec::new();
    for item in vs {
        if ! rv.contains(&item) { 
            rv.push(*item) 
        }
    }
    rv
}

/// Filters a vector `vs` using a predicate `pred` (a function from `i32` to
/// `bool`). Returns a new vector containing only elements that satisfy `pred`.
pub fn filter(vs: &Vec<i32>, pred: &dyn Fn(i32) -> bool) -> Vec<i32> {
    let mut rv: Vec<i32> = Vec::new();
    for item in vs {
        if pred(*item) { rv.push(*item) }
        }
    rv
}
